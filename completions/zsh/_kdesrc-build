#compdef kdesrc-build

# SPDX-FileCopyrightText: 2022 ivan tkachenko <me@ratijas.tk>
#
# SPDX-License-Identifier: GPL-2.0-or-later

_arguments \
  "--pretend[Don't actually take major actions, instead describe what would be done]" \
  '--list-build[List what modules would be built in the order in which they would be built]' \
  '--dependency-tree[Print out dependency information on the modules that would be built, using a `tree` format]' \
  "--no-src[Don't update source code, just build/install]" \
  "--src-only[Only update the source code]" \
  "--refresh-build[Start the build from scratch]" \
  '--rc-file=[Read configuration from filename instead of default]:::_files' \
  "--initial-setup[Installs Plasma env vars (~/.bashrc), required system pkgs, and a base kdesrc-buildrc]" \
  "--resume-from=[Skips modules until just before or after the given package, then operates as normal]:::_kdesrc-build_modules" \
  "--resume-after=[Skips modules until just before or after the given package, then operates as normal]:::_kdesrc-build_modules" \
  "--stop-before=[Stops just before or after the given package is reached]:::_kdesrc-build_modules" \
  "--stop-after=[Stops just before or after the given package is reached]:::_kdesrc-build_modules" \
  "--include-dependencies[Also builds KDE-based dependencies of given modules]" \
  "--no-include-dependencies[Don't builds KDE-based dependencies of given modules]" \
  '--stop-on-failure[Stops the build as soon as a package fails to build]' \
  '*:: :_kdesrc-build_modules'
